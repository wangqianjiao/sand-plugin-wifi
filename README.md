### 项目说明

由于需要在uni-app开发中方便使用WiFi信息获取功能，插件市场又找不到合适的插件，所以自己封装了一个插件方便云打包等操作。

> 插件市场地址：[点击连接](https://ext.dcloud.net.cn/plugin?id=4843)

### 目录结构

- sand-plugin-wifi-android：插件对应安卓原生源码
- sand-plugin-wifi-ios: 插件对应IOS原生源码

## 安卓App使用插件需要的权限
```
 <!--使用网络-->
<uses-permission android:name="android.permission.INTERNET" />
<!--网络状态-->
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
<!--wifi状态权限-->
<uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
<!--位置信息-->
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
<!--如果是安卓10.0，需要后台获取连接的wifi名称则添加进程获取位置信息权限 -->
<uses-permission android:name="android.permission.ACCESS_BACKGROUND_LOCATION" />
<!--android 10.0使用wifi api新添加的权限-->
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
```

## 苹果App使用插件需要的权限
```
1.  网络权限【所有版本】
2.  位置权限【IOS 13.0以上】
3.  添加WiFi能力后再重新自动生成证书，Capabilities -> + Access WIFI Information 【IOS 12.0以上】
```
> 制作自定义调试基座时添加权限：在manifest.json文件源码试图中添加以下信息：

```
"capabilities": {  
    "entitlements": {   // 合并到工程entitlements文件的数据（json格式）  
    },  
    "plists": {         // 合并到工程Info.plist文件的数据（json格式）  
    }  
}
```
例如添加wifi和蓝牙的权限
```
/* ios打包配置 */
"ios" : {
    "capabilities" : {
        "entitlements" : {
            "com.apple.developer.networking.wifi-info" : true
        },
        "plists" : {
            "NSLocationAlwaysAndWhenInUseUsageDescription" : "获取WiFi信息需要的关联权限信息",
            "NSBluetoothPeripheralUsageDescription" : "使用蓝牙功能，用于蓝牙配网",
            "NSBluetoothAlwaysUsageDescription" : "使用蓝牙功能，用于蓝牙配网",
            "NSLocalNetworkUsageDescription" : "获取WiFi信息需要的关联权限信息",
            "NSLocationAlwaysUsageDescription" : "获取WiFi信息需要的关联权限信息",
            "NSLocationWhenInUseUsageDescription" : "获取WiFi信息需要的关联权限信息"
        }
    },
    "privacyDescription" : {
        "NSLocationAlwaysAndWhenInUseUsageDescription" : "获取位置信息",
        "NSBluetoothPeripheralUsageDescription" : "使用蓝牙功能",
        "NSBluetoothAlwaysUsageDescription" : "使用蓝牙功能",
        "NSLocalNetworkUsageDescription" : "使用本地网络",
        "NSLocationAlwaysUsageDescription" : "获取位置信息",
        "NSLocationWhenInUseUsageDescription" : "获取位置信息"
    },
    "idfa" : true
}
```
> 官方文档地址：https://ask.dcloud.net.cn/article/36393

## 使用方法示例(index.vue)
```
<template>
    <div>
        <button type="primary" @click="getWIFIInfo">getWIFIInfo</button>
        <text>展示返回的内容：{{content}}</text>
    </div>
</template>

<script>
  // 首先需要通过 uni.requireNativePlugin("ModuleName") 获取 module 
  var sand = uni.requireNativePlugin("sand-plugin-wifi");
  var page;
	export default {
		data() {
			return {
				content: '请点击按钮获取'
			}
		},
		onLoad() {
          page=this;
		},
		methods: {
          getWIFIInfo() {
            //触发一次位置获取，否则iphone设置中没有定位服务
            uni.getLocation({
                type: 'wgs84',
                success: function (res) {
                    console.log('当前位置的经度：' + res.longitude);
                    console.log('当前位置的纬度：' + res.latitude);
                    page.content+="位置信息：[经度:"+res.longitude+"],[纬度:"+res.latitude+"]"
                }
            });
            // 调用异步方法
            sand.getWIFIInfo({},
                (ret) => {
              //在页面显示获取的结果内容
              page.content+="原始结果："+JSON.stringify(ret);
              page.content+="WiFi信息：[ssid:"+ret.ssid+"],[bssid:"+ret.bssid+"]"
            });
          }
		}
	}
</script>

<style>
	.content {
		display: flex;
		flex-direction: column;
		align-items: center;
		justify-content: center;
	}

	.logo {
		height: 200rpx;
		width: 200rpx;
		margin-top: 200rpx;
		margin-left: auto;
		margin-right: auto;
		margin-bottom: 50rpx;
	}

	.text-area {
		display: flex;
		justify-content: center;
	}

	.title {
		font-size: 36rpx;
		color: #8f8f94;
	}
</style>

```
> 官方使用文档：https://nativesupport.dcloud.net.cn/NativePlugin/use/use

## 接口返回内容(JSON)
```
{
    "status": "状态码",//2500代表成功，其他是失败 
    "ssid": "wifi的名称", 
    "bssid": "wifi的MAC地址", 
    "message": "成功或失败的说明信息"
}
```
例子：
```
{
    "status": "2500",
    "ssid": "my-home-wifi", 
    "bssid": "D0:35:33:32:D1:D9", 
    "message": "获取成功"
}
```
## 预览图

![输入图片说明](preview.PNG)

## 状态码


码值 | 说明
---|---
2500 | 成功
2504 | 获取异常，可能报了异常信息

## 其他常见问题

#### SSID出现(null)
苹果手机权限问题，检查APP是否配置以下权限
```
1.  网络权限【所有版本】
2.  位置权限【IOS 13.0以上】
3.  添加WiFi能力后再重新自动生成证书，Capabilities -> + Access WIFI Information 【IOS 12.0以上】
```


