package com.sand.plugin.wifi;

import android.app.Application;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.alibaba.fastjson.JSONObject;

import io.dcloud.feature.uniapp.annotation.UniJSMethod;
import io.dcloud.feature.uniapp.bridge.UniJSCallback;
import io.dcloud.feature.uniapp.common.UniModule;

import static android.content.Context.WIFI_SERVICE;

public class SandPluginWifi extends UniModule {
    String TAG = "SandPluginWifi";
    public static int REQUEST_CODE = 1000;

    //run ui thread
    @UniJSMethod(uiThread = true)
    public void getWIFIInfo(JSONObject options, UniJSCallback callback) {
        Log.e(TAG, "getWIFIInfo--"+options);
        WifiManager wifiManager = (WifiManager) this.mUniSDKInstance.getContext().getApplicationContext().getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();//返回当前连接wifi的信息
        wifiManager.getScanResults();
        if(callback != null) {
            JSONObject data = new JSONObject();
            data.put("status", "2500");
            data.put("message","获取成功");
            String ssid=wifiInfo.getSSID();
            data.put("ssid",ssid.substring(1,ssid.length()-1));
            data.put("bssid",wifiInfo.getBSSID());
            callback.invoke(data);
            //callback.invokeAndKeepAlive(data);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_CODE && data.hasExtra("respond")) {
            Log.e("SandPluginWifi", "原生页面返回----"+data.getStringExtra("respond"));
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
