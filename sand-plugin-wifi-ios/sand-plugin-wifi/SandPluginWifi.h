//
//  SandPluginWifi.h
//  sand-plugin-wifi
//
//  Created by Qianjiao Wang on 2021/4/20.
//

#import <Foundation/Foundation.h>
#import "DCUniModule.h"

NS_ASSUME_NONNULL_BEGIN

@interface SandPluginWifi : DCUniModule
@end

NS_ASSUME_NONNULL_END
